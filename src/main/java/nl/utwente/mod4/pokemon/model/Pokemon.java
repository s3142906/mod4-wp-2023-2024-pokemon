package nl.utwente.mod4.pokemon.model;

import net.bytebuddy.implementation.bind.annotation.Super;

public class Pokemon extends NamedEntity {
    float height;
    float weight;
    int hp;
    int attk;

    int spattk;
    int def;
    int spdef;
    int sp;

    public Pokemon(String id, float height,float weight, int hp,int attk, int spattk, int def,int spdef, int sp){
        super();
        this.id =id;
        this.height=height;
        this.weight=weight;
        this.attk=attk;
        this.spattk=spattk;
        this.def=def;
        this.spdef=spdef;
        this.sp=sp;
    }
}
