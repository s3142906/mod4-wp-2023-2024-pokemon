package nl.utwente.mod4.pokemon.routes;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import nl.utwente.mod4.pokemon.dao.TrainerDao;
import nl.utwente.mod4.pokemon.model.ResourceCollection;
import nl.utwente.mod4.pokemon.model.Pokemon;
import nl.utwente.mod4.pokemon.model.Trainer;

@Path("/pokemon")
public class PokemonRoute {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResourceCollection<Pokemon> getPokemon(
            @QueryParam("pageSize") int pageSize,
            @QueryParam("pageNumber") int pageNumber
    ){
        // initializes page size and page number for request
        int ps = pageSize > 0 ? pageSize : Integer.MAX_VALUE;
        int pn = pageNumber > 0 ? pageNumber : 1;

        // request the desired page to the Data Access Object
        ArrayList<Pokemon> pokemon = new ArrayList<>();

        pokemon.add(new Pokemon("1",1,1,1,1,1,1,1,1));
        pokemon.add(new Pokemon("2",2,2,2,2,2,2,2,2));
        pokemon.add(new Pokemon("3", 3,3,3,3,3,3,3,3));


        return new ResourceCollection<>(pokemon.toArray(new Pokemon[0]), pokemon.size(), 1, pokemon.size());
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Pokemon getPokemon(@PathParam("id") String id) {
        ArrayList<Pokemon> pokemon = new ArrayList<>();
        pokemon.add(new Pokemon("1",1,1,1,1,1,1,1,1));
        pokemon.add(new Pokemon("2",2,2,2,2,2,2,2,2));
        pokemon.add(new Pokemon("3", 3,3,3,3,3,3,3,3));
        int chosenOne;
        for (Pokemon test: pokemon
             ) {
            if(test.id.equals(id)){
                return test;
            }
        }
        return null;
    }
}
